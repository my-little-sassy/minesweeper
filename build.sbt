import org.scalajs.linker.interface.{ModuleInitializer, ModuleSplitStyle}

ThisBuild / scalaVersion     := "2.13.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "potato"
ThisBuild / organizationName := "potato"

lazy val root = (project in file("."))
  .enablePlugins(ScalaJSPlugin)
  .settings(
    name := "Minesweeper",
    libraryDependencies ++= Seq(
      "org.scala-js"           %%% "scalajs-dom"    % "2.8.0",
//      "org.scala-lang.modules" %%% "scala-xml"      % "2.2.0",
//      "org.typelevel" %%% "cats-core"   % "2.10.0",
//      "org.typelevel" %%% "cats-effect" % "3.5.3",
//      "co.fs2"        %%% "fs2-core"    % "3.9.4",
    ),
//    mainClass := Some("minesweeper.Potato"),
    scalaJSMainModuleInitializer := Some(ModuleInitializer.mainMethod("minesweeper.Potato", "main")),
    scalaJSUseMainModuleInitializer := true,
    scalaJSLinkerConfig ~= {
      _.withModuleKind(ModuleKind.ESModule)
        .withModuleSplitStyle(
          ModuleSplitStyle.SmallModulesFor(List("livechart")))
    },
    scalacOptions ++= Seq("-Ymacro-annotations")
  )

lazy val console = (project in file("console"))
  .dependsOn(root)
