import org.scalajs.dom.Event

import scala.scalajs.js
import scala.scalajs.js.JSConverters._

package object snabb {

  sealed trait Children {
    def render: js.Any
  }
  object Children {

    case object None extends Children {
      override def render: js.Any = js.undefined
    }

    case class Text(value: String) extends Children {
      override def render: js.Any = value
    }

    case class NodeList(nodes: Seq[HNode]) extends Children {
      override def render: js.Any = nodes.map(_.render).toJSArray
    }

  }

  case class HNode(
    selector: String,
    attributes: Seq[Attribute] = Seq.empty,
    properties: Seq[Attribute] = Seq.empty,
    dataset: Seq[Attribute] = Seq.empty,
    eventListeners: Seq[EventListener[_]] = Seq.empty,
    children: Children = Children.None
  ) {

    def attrs(a: Attribute*): HNode = this.copy(attributes = a)

    def props(p: Attribute*): HNode = this.copy(properties = p)

    def dataset(d: Attribute*): HNode = this.copy(dataset = d)

    def on(listeners: EventListener[_]*): HNode = this.copy(eventListeners = listeners)

    def children(nodes: HNode*): HNode = this.copy(children = Children.NodeList(nodes))

    def text(value: String): HNode = this.copy(children = Children.Text(value))

    def render: VNode = {
      val attrs = js.Dynamic.literal()
      attributes.foreach(a => attrs.updateDynamic(a._1)(a._2))

      val ds = js.Dynamic.literal()
      dataset.foreach(d => ds.updateDynamic(d._1)(d._2))

      val listeners = js.Dynamic.literal()
      eventListeners.foreach(l => listeners.updateDynamic(l._1)(l._2))

      val data = js.Dynamic.literal(
        attrs = attrs,
        dataset = ds,
        on = listeners
      )

      Snabbdom.h(selector, data, children.render)
    }

  }

  def h(selector: String): HNode = HNode(selector)

  type EventListener[E <: Event] = (String, E => Unit)

  class EventListenerKey[E <: Event](key: String) {
    def := (f: E => Unit): EventListener[E] = key -> f
  }

  type Attribute = (String, String)

  class AttributeKey(key: String) {
    def := (value: String): Attribute = key -> value
  }

}
