package minesweeper

import org.scalajs.dom.Element

import scala.scalajs.js.|

abstract class App[S, V] {

  type Command = S => S

  def init: S

  def render(state: S, dispatch: Command => Unit): V

  def patch(node: V | Element, vdom: V): V

  def mount(node: V | Element): Unit = {
    var state: S = init
    var view: V | Element = node

    def dispatch(action: Command): Unit = {
      println(s"Dispatched action $action")
      val next = action(state)
      state = next
      view = patch(view, render(state, dispatch))
    }

    patch(view, render(state, dispatch))
  }

}

object App {

  type Mount[V] = V | Element
  type Patch[V] = (Mount[V], V) => V
  type Dispatch[S] = (S => S) => Unit
  type Render[S, V] = (S, Dispatch[S]) => V

  def apply[S, V](
    init: S,
    patch: Patch[V],
    render: Render[S, V],
    mount: Element
  ): Unit = {
    var state: S = init
    var node: V | Element = mount

    def dispatch(action: S => S): Unit = {
      println(s"Dispatched action $action")
      val next = action(state)
      state = next
      node = patch(node, render(state, dispatch))
    }

    node = patch(node, render(state, dispatch))
  }
}
