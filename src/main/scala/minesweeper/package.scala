import minesweeper.game.Action

package object minesweeper {

  type Command = State => State

  def Click(x: Int, y: Int, clickType: ClickType): Command = { state =>
    state.screen match {
      case s @ Screen.Game(game) =>
        val action = clickType match {
          case ClickType.Primary   => Action.Open(x, y)
          case ClickType.Secondary => Action.Flag(x, y)
          case ClickType.Extra     => Action.OpenNeighbours(x, y)
        }
        State(s.copy(game = game.update(action)))

      case _ => state
    }
  }

  def NewGame(): Command = { state =>
    state.copy(screen = Screen.Start)
  }

  sealed trait ClickType
  object ClickType {
    case object Primary extends ClickType
    case object Secondary extends ClickType
    case object Extra extends ClickType

    def from(button: Int, detail: Int): ClickType = (button, detail) match {
      case (0, 1) => Primary
      case (0, _) => Extra
      case (2, 1) => Secondary
    }
  }

}
