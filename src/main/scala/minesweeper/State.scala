package minesweeper

import minesweeper.game.GameState

case class State(screen: Screen)

sealed trait Screen
object Screen {

  case object Start extends Screen

  case class Game(game: GameState) extends Screen

}
