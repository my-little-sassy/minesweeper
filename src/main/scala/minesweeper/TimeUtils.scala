package minesweeper

object TimeUtils {

  private val Seconds = 1000
  private val Minutes = 60 * Seconds
  private val Hours = 60 * Minutes
  private val TimeUnits = Seq("h", "m", "s")

  def now: Long = System.currentTimeMillis()

  def timeSince(when: Long): Long = now - when

  def pretty(time: Long): String = Seq(time / Hours, time / Minutes, time / Seconds)
    .zip(TimeUnits)
    .dropWhile(_._1 <= 0)
    .map { case (t, u) => s"$t$u" }
    .mkString(" ")

}
