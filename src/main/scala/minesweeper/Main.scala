package minesweeper

import minesweeper.view.StateView
import org.scalajs.dom
import snabb.{Snabbdom, VNode}

import scala.scalajs.js

object Main {

  def main(args: Array[String]): Unit = {
    println("Potato!")

    val _patch = Snabbdom.init(js.Array(
      Snabbdom.classModule,
      Snabbdom.propsModule,
      Snabbdom.styleModule,
      Snabbdom.datasetModule,
      Snabbdom.eventListenersModule,
    ))

//    val app = new App[State, VNode] {
//
//      override def init: State = State(Screen.Start)
//
//      override def render(state: State, dispatch: Command => Unit): VNode = {
//        println(s"Render state $state")
//        StateView.render(state, dispatch)
//      }
//
//      override def patch(node: VNode | Element, vdom: VNode): VNode = _patch(node, vdom)
//
//    }

//    app.mount(dom.document.querySelector("#app"))

    App[State, VNode](
      init   = State(Screen.Start),
      patch  = _patch,
      render = StateView(_, _).render,
      mount  = dom.document.querySelector("#app")
    )
  }

}
