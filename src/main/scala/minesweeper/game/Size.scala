package minesweeper.game

case class Size(width: Int, height: Int, mines: Int)

object Size {
  val Small: Size  = Size(10, 10, 9)
  val Medium: Size = Size(15, 10, 15)
  val Large: Size  = Size(25, 15, 60)
  val Huge: Size   = Size(32, 18, 120)
}
