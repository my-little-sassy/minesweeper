package minesweeper.game

package object generator {

  type Generator = (Size, Int, Int) => Board

}
