package minesweeper.game.generator

import minesweeper.game.{Board, Cell, CellState, Point, Size}

import scala.util.Random

object Classic extends Generator {

  override def apply(size: Size, x: Int, y: Int): Board = {
    def getCell(cs: Seq[Cell], x: Int, y: Int): Cell = {
      cs(index(x, y))
    }

    def index(x: Int, y: Int): Int = y * size.width + x

    val cells = for {
      y <- 0 until size.height
      x <- 0 until size.width
    } yield (x, y)

    val mines = Random.shuffle(cells.filter(_.distance((x, y)) >= 2)).take(size.mines)

    val withMines = cells.map { cell =>
      val (xx, yy) = cell
      val state =
        if (x == xx && y == yy) CellState.Open
        else CellState.Hidden
      Cell(xx, yy, mines.contains(cell), state)
    }.toVector

    val withNeighbours = withMines
      .map { cell =>
        val neighbourMines = for {
          y <- math.max(0, cell.y - 1) to math.min(cell.y + 1, size.height - 1)
          x <- math.max(0, cell.x - 1) to math.min(cell.x + 1, size.width - 1)
          c = getCell(withMines, x, y)
          if c != cell && c.mine
        } yield (x, y)
        cell.copy(neighbours = neighbourMines.size)
      }

    val board = Board(size, withNeighbours)
    val start = board(x, y)
    if (start.neighbours == 0) board.openNeighbours(x, y)
    else board
  }

}
