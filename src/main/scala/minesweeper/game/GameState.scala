package minesweeper.game

import minesweeper.TimeUtils

sealed trait GameState {

  protected def performUpdate: PartialFunction[Action, GameState]

  def update(action: Action): GameState = (performUpdate orElse noAction)(action)

  private def noAction: PartialFunction[Action, GameState] = {
    case _ => this
  }

}

object GameState {

  case class NewGame(size: Size) extends GameState {
    override protected def performUpdate: PartialFunction[Action, GameState] = {
      case Action.Open(x, y) => OnGoing(TimeUtils.now, Board.generate(size, x, y))
    }
  }

  case class OnGoing(startedAt: Long, board: Board) extends GameState {
    override protected def performUpdate: PartialFunction[Action, GameState] = {
      case Action.Open(x, y) if board(x, y).state == CellState.Hidden =>
        val updatedBoard = board.open(x, y)
        if (updatedBoard.isGameOver) GameOver(startedAt, TimeUtils.now, updatedBoard.endGame())
        else this.copy(board = updatedBoard)

      case Action.OpenNeighbours(x, y) =>
        val updatedBoard =
          if (board.neighbours(x, y).count(_.state == CellState.Flag) == board(x, y).neighbours) board.openNeighbours(x, y)
          else board
        if (updatedBoard.isGameOver) GameOver(startedAt, TimeUtils.now, updatedBoard.endGame())
        else this.copy(board = updatedBoard)

      case Action.Flag(x, y) =>
        this.copy(board = board.flag(x, y))

      case _ => this
    }
  }

  case class GameOver(startedAt: Long, endedAt: Long, board: Board) extends GameState {
    override protected def performUpdate: PartialFunction[Action, GameState] = {
      case Action.NewGame(size) => NewGame(size)
    }
  }

}
