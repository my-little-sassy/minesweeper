package minesweeper

package object game {

  case class Cell(x: Int, y: Int, mine: Boolean, state: CellState, neighbours: Int = 0)

  sealed trait CellState
  object CellState {
    case object Hidden extends CellState
    case object Flag extends CellState
    case object Open extends CellState
  }

  sealed trait Action
  object Action {
    case class Open(x: Int, y: Int) extends Action
    case class OpenNeighbours(x: Int, y: Int) extends Action
    case class Flag(x: Int, y: Int) extends Action
    case class NewGame(size: Size) extends Action
  }

  implicit class Point(p: (Int, Int)) {
    def distance(other: (Int, Int)): Int = {
      val (x1, y1) = p
      val (x2, y2) = other
      math.sqrt(math.pow(x1 - x2, 2) + math.pow(y1 - y2, 2)).toInt
    }
  }

  implicit class IntOps(val x: Int) extends AnyVal {
    @inline
    def between(a: Int, b: Int): Boolean = x >= a && x <= b
  }

}
