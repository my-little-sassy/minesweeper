package minesweeper.game

import minesweeper.game.generator.{Classic, Generator}

import scala.annotation.tailrec

case class Board(size: Size, cells: Seq[Cell] = Vector.empty) {

  private def index(x: Int, y: Int): Int = y * size.width + x

  def apply(x: Int, y: Int): Cell = cells(index(x, y))

  def apply(cell: Cell): Cell = apply(cell.x, cell.y)

  def open(x: Int, y: Int): Board = {
    val cell = apply(x, y).copy(state = CellState.Open)
    val updated =
      if (!cell.mine && cell.neighbours == 0) openNeighbours(cells.updated(index(x, y), cell), Seq(cell), Set(cell))
      else cells.updated(index(x, y), cell)
    this.copy(cells = updated)
  }

  def flag(x: Int, y: Int): Board = {
    val cell = apply(x, y)
    val state = cell.state match {
      case CellState.Flag   => CellState.Hidden
      case CellState.Hidden => CellState.Flag
      case other            => other
    }
    this.copy(cells = cells.updated(index(x, y), cell.copy(state = state)))
  }

  lazy val hasOpenMine: Boolean = cells.exists(c => c.state == CellState.Open && c.mine)

  lazy val isGameOver: Boolean = {
    hasOpenMine || minesLeft == cells.count(_.state == CellState.Hidden)
  }

  lazy val minesLeft: Int = size.mines - cells.count(_.state == CellState.Flag)

  def openNeighbours(x: Int, y: Int): Board = {
    val cell = apply(x, y)
    val updated = openNeighbours(cells, Seq(cell), Set(cell))
    this.copy(cells = updated)
  }

  def endGame(): Board = {
    def setHiddenTo(state: CellState): PartialFunction[Cell, Cell] = {
      case c @ Cell(_, _, true, CellState.Hidden, _) => c.copy(state = state)
      case other => other
    }

    val update =
      if (hasOpenMine) setHiddenTo(CellState.Open)
      else setHiddenTo(CellState.Flag)

    val updated = cells.map(update)
    this.copy(cells = updated)
  }

  def neighbours(x: Int, y: Int): Seq[Cell] = {
    cells.filter { cc =>
      cc.x.between(x - 1, x + 1) && cc.y.between(y - 1, y + 1) && !(x == cc.x && y == cc.y)
    }
  }

  @tailrec
  private def openNeighbours(cs: Seq[Cell], toVisit: Seq[Cell], visited: Set[Cell] = Set.empty): Seq[Cell] = {
    def neighbours(cs: Seq[Cell], c: Cell): Seq[Cell] = cs.filter { cc =>
      cc.x.between(c.x - 1, c.x + 1) && cc.y.between(c.y - 1, c.y + 1) && !(c.x == cc.x && c.y == cc.y)
    }

    toVisit.headOption match {
      case None     => cs
      case Some(c) =>
        val ns = neighbours(cs, c)
          .filter(cc => cc.state == CellState.Hidden && !visited.contains(cc))

        val cs2 = cs.map { cc =>
          ns.find(_ == cc) match {
            case Some(_) => cc.copy(state = CellState.Open)
            case None    => cc
          }
        }

        openNeighbours(cs2, toVisit.tail ++ ns.filter(_.neighbours == 0), visited ++ ns)
    }
  }
}

object Board {

  def generate(size: Size, x: Int, y: Int, generator: Generator = Classic): Board = {
    generator(size, x, y)
  }

}
