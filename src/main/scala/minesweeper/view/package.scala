package minesweeper

import minesweeper.game.{Board, Cell, CellState, GameState, Size}
import org.scalajs.dom.{HTMLElement, MouseEvent}
import snabb.HNode
import snabb.events.{click, contextMenu}
import snabb.html._

package object view {

  type Dispatch[S] = (S => S) => Unit

  val StateView: Render[State, State, HNode] = (state: State, dispatch: Dispatch[State]) => ScreenView(state.screen, dispatch)

  val ScreenView: Render[State, Screen, HNode] = new Render[State, Screen, HNode] {

    private def startGame(size: Size): Command = { _ =>
      State(Screen.Game(GameState.NewGame(size)))
    }

    override def apply(screen: Screen, dispatch: Dispatch[State]): HNode = screen match {
      case Screen.Start =>
        div(".screen").children(
          h1(".title").text("Start new game"),
          div(".buttons").children(
            button()
              .on(click := (_ => dispatch(startGame(Size.Small))))
              .text("Small"),
            button()
              .on(click := (_ => dispatch(startGame(Size.Medium))))
              .text("Medium"),
            button()
              .on(click := (_ => dispatch(startGame(Size.Large))))
              .text("Large"),
            button()
              .on(click := (_ => dispatch(startGame(Size.Huge))))
              .text("Huge"),
          )
        )

      case Screen.Game(game) =>
        div(".screen").children(
          GameView(game, dispatch)
        )
    }
  }

  val GameView: Render[State, GameState, HNode] = new Render[State, GameState, HNode] {
    override def apply(state: GameState, dispatch: Dispatch[State]): HNode = {
      state match {
        case GameState.NewGame(size) =>
          val cells = for {
            y <- 0 until size.height
            x <- 0 until size.width
          } yield Cell(x, y, false, CellState.Hidden)
          val board = Board(size, cells)

          renderPanel(GameTime.NotStarted, board, dispatch)

        case GameState.OnGoing(startedAt, board) =>
          renderPanel(GameTime.Running(startedAt), board, dispatch)

        case GameState.GameOver(startedAt, endedAt, board) =>
          renderPanel(GameTime.Ended(startedAt, endedAt), board, dispatch)
      }
    }

    private def renderPanel(time: GameTime, board: Board, dispatch: Dispatch[State]): HNode = {
      div(".panel").children(
        renderFrame(time, board),
        renderBoard(board, dispatch),
        renderFooter(board, dispatch),
      )
    }

    private def renderFrame(gameTime: GameTime, board: Board): HNode = {
      val minesLeft = board.minesLeft
      div(".frame").children(
        span().text(s"Mines left: $minesLeft")
      )
    }

    private def renderBoard(board: Board, dispatch: Dispatch[State]): HNode = {
      val height = s"height-${board.size.height}"
      val width = s"width-${board.size.width}"

      val onClick = { (e: MouseEvent) =>
        e.preventDefault()
        val dataset = e.target.asInstanceOf[HTMLElement].dataset
        val action = for {
          x <- dataset("x").toIntOption
          y <- dataset("y").toIntOption
          _ =  println(s"Clicked on ($x, $y)")
        } yield Click(x, y, ClickType.from(e.button, e.detail))
        action.foreach(dispatch)
      }

      div(s".board.$height.$width")
        .on(
          click       := onClick,
          contextMenu := onClick,
        )
        .children(board.cells.map(renderCell): _*)
    }

    private def renderCell(cell: Cell): HNode = {
      val classes = cell.state match {
        case CellState.Hidden =>
          Seq("hidden")
        case CellState.Flag =>
          Seq("flag")
        case CellState.Open =>
          Seq(
            Some("open"),
            Option.when(cell.mine)("mine"),
            Option.when(!cell.mine && cell.neighbours > 0)(s"neighbours-${cell.neighbours}")
          ).flatten
      }
      val text = Option.when(cell.state == CellState.Open && !cell.mine && cell.neighbours > 0)(cell.neighbours)

      val node = div(s".cell.${classes.mkString(".")}")
        .dataset(
          "x" -> cell.x.toString,
          "y" -> cell.y.toString,
        )

      text.map(t => node.text(t.toString)).getOrElse(node)
    }

    private def renderFooter(board: Board, dispatch: Dispatch[State]): HNode = {
      board match {
        case b if b.isGameOver =>
          div(".footer").children(
            div(".buttons").children(
              button()
                .on(click := { _ => dispatch(NewGame()) })
                .text("New Game")
            )
          )

        case _ =>
          div(".footer")
      }
    }
  }

  sealed trait GameTime {
    def text: String
  }
  object GameTime {

    case object NotStarted extends GameTime {
      override def text: String = "0"
    }

    case class Running(since: Long) extends GameTime {
      override def text: String = TimeUtils.pretty(TimeUtils.timeSince(since))
    }

    case class Ended(since: Long, until: Long) extends GameTime {
      override def text: String = TimeUtils.pretty(until - since)
    }

  }

}
