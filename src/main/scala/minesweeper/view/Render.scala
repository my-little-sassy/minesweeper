package minesweeper.view

trait Render[GS, LS, V] extends ((LS, Dispatch[GS]) => V) {
  def apply(state: LS, dispatch: Dispatch[GS]): V
}
