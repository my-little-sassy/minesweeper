package minesweeper.console

import minesweeper.game._

import scala.annotation.tailrec
import scala.io.StdIn

object Console {

  def main(args: Array[String]): Unit = {
    val game = GameState.NewGame(Size.Small)

    println(show(game))
    loop(game)
  }

  @tailrec
  def loop(game: GameState): Unit = {
    val move = readMove
    val updated = game.update(move)
    println(show(updated))
    loop(updated)
  }

  def readMove: Action = {
    val raw = StdIn.readLine()
    raw.split(" ") match {
      case Array("F", p) =>
        val Array(x, y) = p.split(",").map(_.trim.toInt)
        Action.Flag(x, y)
      case Array("O", p) =>
        val Array(x, y) = p.split(",").map(_.trim.toInt)
        Action.Open(x, y)
      case Array("N", p) =>
        val Array(x, y) = p.split(",").map(_.trim.toInt)
        Action.OpenNeighbours(x, y)
      case Array("R", s) =>
        val size = s match {
          case "S" => Size.Small
          case "M" => Size.Medium
          case "L" => Size.Large
        }
        Action.NewGame(size)
    }
  }

  def show(game: GameState): String = {
    game match {
      case GameState.NewGame(size) =>
        val cells = for {
          y <- 0 until size.height
          x <- 0 until size.width
        } yield Cell(x, y, false, CellState.Hidden)
        show(Board(size, cells)) ++ s"\nMines: ${size.mines}"

      case GameState.OnGoing(_, board) =>
        show(board) ++ s"\nMines: ${board.minesLeft}"

      case GameState.GameOver(_, _, board) =>
        show(board) ++ "\n\nGame over!\n"
    }
  }

  def show(board: Board): String = {

    def showCell(cell: Cell): String = cell match {
      case Cell(x, y, _, CellState.Flag, _)      => " F "
      case Cell(x, y, _, CellState.Hidden, _)    => " X "
      case Cell(x, y, true, CellState.Open, _)   => "[*]"
      case Cell(x, y, false, CellState.Open, 0)  => "[ ]"
      case Cell(x, y, false, CellState.Open, nm) => s"[$nm]"
    }

    def showRow(row: (Seq[String], Int)): String = {
      val (cells, index) = row
      s"$index ${cells.mkString}"
    }

    val header = (0 until board.size.width).mkString(" " * 3, " " * 2, "")

    header ++ "\n" ++
    board.cells
      .map(showCell)
      .grouped(board.size.width)
      .zipWithIndex
      .map(showRow)
      .mkString("\n")
  }

}
